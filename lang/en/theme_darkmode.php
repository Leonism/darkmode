<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Necessary language settings.
 *
 * @package   theme_darkmode
 * @copyright 2021 Mohamed Leon Ankar
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Darkmode';
$string['region-side-pre'] = 'Right';
$string['choosereadme'] = 'Darkmode theme is a child theme of Boost.';

// Settings Page
// Name of the settings pages.
$string['configtitle'] = 'Darkmode Theme settings';
// Name of the first settings tab.
$string['generaltabtitle'] = 'General';

$string['samecolors'] = 'Uniformed colors';

$string['samecolors_desc'] = 'Use light mode\'s primary and secondary colors for dark mode.';

$string['lighttabtitle'] = 'Light Mode';

$string['primarycolor'] = 'Primary color';
// The brand colour setting description.

$string['primarycolorlight_desc'] = "The accent color for light mode.
									 This is basically the color of the links which is most of the text that are not in buttons.
									 This color does not affect text on buttons or interactable objects.
									 INFO: you can change the primary color for dark mode independently in the Dark Mode Colors tab.";
// A description shown in the admin theme selector.


$string['secondarycolor'] = 'Secondary color';
// The brand colour setting description.

$string['secondarycolorlight_desc'] = "This is the color of buttons and interactable objects.
									  INFO: you can change the secondary color for dark mode independently in the Dark Mode Colors tab.";
$string['darktabtitle'] = 'Dark Mode';


$string['primarycolordark_desc'] = "The accent color for dark mode.
									 This is basically the color of the links which is most of the text that are not in buttons.
									 This color does not affect text on buttons or interactable objects.
									 INFO: you can change the primary color for light mode independently in the Light Mode Colors tab.";

$string['secondarycolordark_desc'] = "This is the color of buttons and interactable objects.
									 INFO: you can change the secondary color for light mode independently in the Light Mode Colors tab.";

$string['presetlight'] = 'Light mode preset';

$string['presetlight_desc'] = 'You can choose one of the already added presets for light mode. Boost is the default preset of moodle.';

$string['presetdark'] = 'Dark mode preset';

$string['presetdark_desc'] = 'You can choose one of the already added presets for dark mode.';

// Privacy API strings.
$string['privacy:metadata:tool_usertours_tour_completion_time'] = 'The most recent theme mode chosen by the user.';
