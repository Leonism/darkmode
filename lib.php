<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main functionality of the theme.
 *
 * @package   theme_darkmode
 * @copyright 2021 Mohamed Leon Ankar
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// Use already included compiler to compile SCSS to css.
use \ScssPhp\ScssPhp\Compiler;

/**
 * Prepare the 1st preset scss with the chosen primary and secondary colors.
 * Then do the same thing to the 2nd chosen preset and combine both to end up in the same stylingsheet at the end.
 * While returning a compiling will happen, which will be triggered automatically.
 * The last compiling step will ensure applying the last needed nested rules to be able to switch between 2 themes on the fly.
 *
 * @author Mohamed Leon Ankar
 * @return String The compiled CSS of the 1st theme and 2nd theme with nested rules
 */
function theme_darkmode_get_main_scss_content() {

    global $CFG;

    $comp = new compiler();

    // Get primary and secondary colors from settings.
    $primary = get_config('theme_darkmode')->primarycolor_light;
    $secondary = get_config('theme_darkmode')->secondarycolor_light;

    $darkpreset = get_config('theme_darkmode')->presetdark;
    $lightpreset = get_config('theme_darkmode')->presetlight;

    $lightscss = "\$primary: ".$primary.";
        \$secondary: ".$secondary.";";

    // Load toggler style.
    $toggler = file_get_contents($CFG->dirroot . '/theme/darkmode/scss/post.scss');

    // Load light mode preset.
    $lightscss .= file_get_contents($CFG->dirroot . '/theme/darkmode/scss/' . $lightpreset . '.scss') . $toggler;

    // Start working on dark mode.
    // If admin did not select using same colors in light and dark mode.
    if (!(get_config('theme_darkmode')->samecolors)) {

        $primary = get_config('theme_darkmode')->primarycolor_dark;
        $secondary = get_config('theme_darkmode')->secondarycolor_dark;

        $scss = "\$primary: ".$primary.";
        \$secondary: " . $secondary . ";";

    } else {

        // If light and dark modes use the same colors.

        $scss = "\$primary: " . $primary . ";
            \$secondary: " . $secondary . ";
            \$link-color: \$primary;";
    }

    // Load dark mode preset.
    $scss .= file_get_contents($CFG->dirroot . '/theme/darkmode/scss/' . $darkpreset . '.scss') . $toggler;

    $path = $CFG->dirroot . '/theme/darkmode/scss/' . $darkpreset . '.scss';

    // Compile dark preset.
    $darkcompiled = $comp->compile($scss, $path);

    // Use nested rules in SCSS to build new rules.
    // [data-theme="dark"] is an attributed added to the HTML tag using JavaScript in control.js.
    return $lightscss . '[data-theme="dark"] {' . $darkcompiled .'}';

};
