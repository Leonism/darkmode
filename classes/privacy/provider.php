<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Privacy API.
 *
 * @package   theme_darkmode
 * @copyright 2021 Mohamed Leon Ankar
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_darkmode\privacy;

defined('MOODLE_INTERNAL') || die();

use core_privacy\local\metadata\collection;
use core_privacy\local\request\user_preference_provider;

/**
 * The class for using the privacy API.
 *
 * @author Mohamed Leon Ankar
 */
class provider implements
        // This plugin does store personal user data.
        \core_privacy\local\metadata\provider {

    /**
     * Describing the type of data stored.
     *
     * @author Mohamed Leon Ankar
     * @param collection $collection The object to hold metadata.
     * @return collection $collection holding all the added data needed to describe the use of data.
     */
    public static function get_metadata(collection $collection) : collection {

        // Here you will add more items into the collection.
        $collection->add_user_preference('tool_usertours_tour_completion_time',
        'privacy:metadata:preference:tool_usertours_tour_completion_time');

        return $collection;
    }
}
